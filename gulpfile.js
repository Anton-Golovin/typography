'use strict';

var gulp = require('gulp');
var sass = require('gulp-sass');

gulp.task('default', function () {
    return gulp.src('./private/typography.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest('./public/'));
});

gulp.task('sass:watch', function () {
    gulp.watch('./private/typography.scss', ['sass']);
});
